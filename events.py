#!/usr/bin/env python
# -*- coding: utf-8 -*-

import OPi.GPIO as GPIO
from time import sleep

def EventReadPin(channel):
    sleep(0.1)
    print('Event channel: ' + str(channel) + ' received')

def InitPinEvent(pins):
    for i in pins:
        GPIO.setup(i, GPIO.IN)
        GPIO.add_event_detect(i, GPIO.BOTH)
        GPIO.add_event_callback(i, EventReadPin)

def InitPinOut(pin):
    GPIO.setup(pin, GPIO.OUT)

def SetPinOut(pin, value):
    GPIO.output(pin, value)

if __name__ == '__main__':
    print('Intercom started')

    try:
        # H5
        GPIO.setboard(GPIO.PC2)
        EventPins = [3, 5, 8, 10, 11, 12, 13, 15, 16, 18, 19, 21, 22, 23, 24, 26, 27, 28, 29, 31, 32, 33, 35, 36, 37, 38, 40]

        # H3
        #GPIO.setboard(GPIO.PLUS2E)
        #EventPins = [11, 13, 15, 16, 18, 22, 26, 29, 31, 32, 33, 35, 36, 37, 38, 40] # 7
        #             1   0   3           2  21   7   8 200      10 201  20 198 199    6

        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)

        InitPinEvent(EventPins)

        OutPin = 7
        InitPinOut(OutPin)

        while True:
            SetPinOut(OutPin, 1)
            sleep(1)
            SetPinOut(OutPin, 0)
            sleep(1)

        print('Test stopped')

    except KeyboardInterrupt:
        print('\nTest stopped')

    except Exception, e:
        print('Test stopped after exception: ' + str(e))

    finally:
        GPIO.cleanup()
