#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>

int main(int argc, char * argv[])
{
  int OUT_PIN = 16;

  if (wiringPiSetup() < 0)  {
    fprintf(stderr, "Unable to setup wiringPi: %s\n", strerror (errno));
    return 1;
  }

  pinMode(OUT_PIN, OUTPUT);

  for (;;) {
    digitalWrite(OUT_PIN, LOW);
    delay(1000);
    digitalWrite(OUT_PIN, HIGH);
    delay(1000);
  }

  return 0;
}
