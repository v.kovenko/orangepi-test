#!/usr/bin/env python
# -*- coding: utf-8 -*-

import OPi.GPIO as GPIO
from time import sleep

def InitPinOut(pins):
    for i in pins:
        GPIO.setup(i, GPIO.OUT)

def SetPinOut(pins, value):
    for i in pins:
        GPIO.output(i, value)

if __name__ == '__main__':
    print('Intercom started')

    try:
        # H5
        GPIO.setboard(GPIO.PC2)
        pins = [3, 5, 7, 8, 10, 11, 12, 13, 15, 16, 18, 19, 21, 22, 23, 24, 26, 27, 28, 29, 31, 32, 33, 35, 36, 37, 38, 40]

        # H3
        #GPIO.setboard(GPIO.PLUS2E)
        #pins = [7, 11, 12, 13, 15, 16, 18, 22, 26, 29, 31, 32, 33, 35, 36, 37, 38, 40]

        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)

        InitPinOut(pins)

        while True:
            SetPinOut(pins, 1)
            sleep(1)
            SetPinOut(pins, 0)
            sleep(1)

        print('Test stopped')

    except KeyboardInterrupt:
        print('\nTest stopped')

    except Exception, e:
        print('Test stopped after exception: ' + str(e))

    finally:
        GPIO.cleanup()
